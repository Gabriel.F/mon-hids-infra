FROM tutum/apache-php
LABEL maintainer Gabriel Fruhauf

COPY sources.list /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y software-properties-common 
RUN add-apt-repository ppa:certbot/certbot
RUN apt-get update
RUN apt-get install python-certbot-apache git
RUN certbot --apache
RUN mkdir -p /var/www/hids-admin && cd /var/www/hids-admin && git pull https://gitlab.com/chevallm2/mon-hids.git
RUN adduser -D hids
RUN echo hids:nbvcxwmyhids | chpasswd
